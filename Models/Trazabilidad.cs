//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PrimerProyecto.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trazabilidad
    {
        public long id { get; set; }
        public string Tipo { get; set; }
        public int Kilos { get; set; }
        public int Teus { get; set; }
        public string Competencia { get; set; }
        public string Otra_Competencia { get; set; }
        public string Cod_Envio { get; set; }
        public string Pais_Origen { get; set; }
        public string Ciudad_Origen { get; set; }
        public string Pais_Destino { get; set; }
        public string Cuidad_Destino { get; set; }
        public string Via { get; set; }
        public long idProcesos { get; set; }
        public long idEmbarques { get; set; }
    
        public virtual Embarques Embarques { get; set; }
        public virtual Procesos Procesos { get; set; }
    }
}
