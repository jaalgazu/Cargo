﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PrimerProyecto.Models;

namespace PrimerProyecto.Controllers
{
    public class TrazabilidadsController : Controller
    {
        private PrimerProyectoEntities db = new PrimerProyectoEntities();

        // GET: Trazabilidads
        public ActionResult Index()
        {
            var trazabilidad = db.Trazabilidad.Include(t => t.Embarques).Include(t => t.Procesos);
            return View(trazabilidad.ToList());
        }

        // GET: Trazabilidads/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trazabilidad trazabilidad = db.Trazabilidad.Find(id);
            if (trazabilidad == null)
            {
                return HttpNotFound();
            }
            return View(trazabilidad);
        }

        // GET: Trazabilidads/Create
        public ActionResult Create()
        {
            ViewBag.idEmbarques = new SelectList(db.Embarques, "id", "Direccion_pro");
            ViewBag.idProcesos = new SelectList(db.Procesos, "id", "Nombre");
            return View();
        }

        // POST: Trazabilidads/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Tipo,Kilos,Teus,Competencia,Otra_Competencia,Cod_Envio,Pais_Origen,Ciudad_Origen,Pais_Destino,Cuidad_Destino,Via,idProcesos,idEmbarques")] Trazabilidad trazabilidad)
        {
            if (ModelState.IsValid)
            {
                db.Trazabilidad.Add(trazabilidad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEmbarques = new SelectList(db.Embarques, "id", "Direccion_pro", trazabilidad.idEmbarques);
            ViewBag.idProcesos = new SelectList(db.Procesos, "id", "Nombre", trazabilidad.idProcesos);
            return View(trazabilidad);
        }

        // GET: Trazabilidads/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trazabilidad trazabilidad = db.Trazabilidad.Find(id);
            if (trazabilidad == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEmbarques = new SelectList(db.Embarques, "id", "Direccion_pro", trazabilidad.idEmbarques);
            ViewBag.idProcesos = new SelectList(db.Procesos, "id", "Nombre", trazabilidad.idProcesos);
            return View(trazabilidad);
        }

        // POST: Trazabilidads/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Tipo,Kilos,Teus,Competencia,Otra_Competencia,Cod_Envio,Pais_Origen,Ciudad_Origen,Pais_Destino,Cuidad_Destino,Via,idProcesos,idEmbarques")] Trazabilidad trazabilidad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trazabilidad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEmbarques = new SelectList(db.Embarques, "id", "Direccion_pro", trazabilidad.idEmbarques);
            ViewBag.idProcesos = new SelectList(db.Procesos, "id", "Nombre", trazabilidad.idProcesos);
            return View(trazabilidad);
        }

        // GET: Trazabilidads/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trazabilidad trazabilidad = db.Trazabilidad.Find(id);
            if (trazabilidad == null)
            {
                return HttpNotFound();
            }
            return View(trazabilidad);
        }

        // POST: Trazabilidads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Trazabilidad trazabilidad = db.Trazabilidad.Find(id);
            db.Trazabilidad.Remove(trazabilidad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
