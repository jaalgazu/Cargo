﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PrimerProyecto.Models;

namespace PrimerProyecto.Controllers
{
    public class EmbarquesController : Controller
    {
        private PrimerProyectoEntities db = new PrimerProyectoEntities();

        // GET: Embarques
        public ActionResult Index()
        {
            return View(db.Embarques.ToList());
        }

        // GET: Embarques/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Embarques embarques = db.Embarques.Find(id);
            if (embarques == null)
            {
                return HttpNotFound();
            }
            return View(embarques);
        }

        // GET: Embarques/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Embarques/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Direccion_pro,Telefono_pro,Contacto_pro,N_Orden,Consignatario,Origen,Destino,Embarque_A,Embarque_M,Producto,Zona_Aduanera,Incoterm,Descripcion")] Embarques embarques)
        {
            if (ModelState.IsValid)
            {
                db.Embarques.Add(embarques);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(embarques);
        }

        // GET: Embarques/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Embarques embarques = db.Embarques.Find(id);
            if (embarques == null)
            {
                return HttpNotFound();
            }
            return View(embarques);
        }

        // POST: Embarques/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Direccion_pro,Telefono_pro,Contacto_pro,N_Orden,Consignatario,Origen,Destino,Embarque_A,Embarque_M,Producto,Zona_Aduanera,Incoterm,Descripcion")] Embarques embarques)
        {
            if (ModelState.IsValid)
            {
                db.Entry(embarques).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(embarques);
        }

        // GET: Embarques/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Embarques embarques = db.Embarques.Find(id);
            if (embarques == null)
            {
                return HttpNotFound();
            }
            return View(embarques);
        }

        // POST: Embarques/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Embarques embarques = db.Embarques.Find(id);
            db.Embarques.Remove(embarques);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
