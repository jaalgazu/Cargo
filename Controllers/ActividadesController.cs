﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PrimerProyecto.Models;

namespace PrimerProyecto.Controllers
{
    public class ActividadesController : Controller
    {
        private PrimerProyectoEntities1 db = new PrimerProyectoEntities1();

        // GET: Actividades
        public ActionResult Index()
        {
            var actividades = db.Actividades.Include(a => a.Procesos).Include(a => a.Usuarios).Include(a => a.Usuarios1);
            return View(actividades.ToList());
        }

        // GET: Actividades/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actividades actividades = db.Actividades.Find(id);
            if (actividades == null)
            {
                return HttpNotFound();
            }
            return View(actividades);
        }

        // GET: Actividades/Create
        public ActionResult Create()
        {
            ViewBag.Relacionado_con = new SelectList(db.Procesos, "id", "Nombre");
            ViewBag.Asignada_por = new SelectList(db.Usuarios, "id", "Nombre");
            ViewBag.Creada_por = new SelectList(db.Usuarios, "id", "Nombre");
            return View();
        }

        // POST: Actividades/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Creada_por,Asignada_por,Relacionado_con,Asunto,Cliente,Vencimiento,Estado,Prioridad,Fecha_inicio,Fecha_final")] Actividades actividades)
        {
            if (ModelState.IsValid)
            {
                db.Actividades.Add(actividades);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Relacionado_con = new SelectList(db.Procesos, "id", "Nombre", actividades.Relacionado_con);
            ViewBag.Asignada_por = new SelectList(db.Usuarios, "id", "Nombre", actividades.Asignada_por);
            ViewBag.Creada_por = new SelectList(db.Usuarios, "id", "Nombre", actividades.Creada_por);
            return View(actividades);
        }

        // GET: Actividades/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actividades actividades = db.Actividades.Find(id);
            if (actividades == null)
            {
                return HttpNotFound();
            }
            ViewBag.Relacionado_con = new SelectList(db.Procesos, "id", "Nombre", actividades.Relacionado_con);
            ViewBag.Asignada_por = new SelectList(db.Usuarios, "id", "Nombre", actividades.Asignada_por);
            ViewBag.Creada_por = new SelectList(db.Usuarios, "id", "Nombre", actividades.Creada_por);
            return View(actividades);
        }

        // POST: Actividades/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Creada_por,Asignada_por,Relacionado_con,Asunto,Cliente,Vencimiento,Estado,Prioridad,Fecha_inicio,Fecha_final")] Actividades actividades)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actividades).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Relacionado_con = new SelectList(db.Procesos, "id", "Nombre", actividades.Relacionado_con);
            ViewBag.Asignada_por = new SelectList(db.Usuarios, "id", "Nombre", actividades.Asignada_por);
            ViewBag.Creada_por = new SelectList(db.Usuarios, "id", "Nombre", actividades.Creada_por);
            return View(actividades);
        }

        // GET: Actividades/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actividades actividades = db.Actividades.Find(id);
            if (actividades == null)
            {
                return HttpNotFound();
            }
            return View(actividades);
        }

        // POST: Actividades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Actividades actividades = db.Actividades.Find(id);
            db.Actividades.Remove(actividades);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
