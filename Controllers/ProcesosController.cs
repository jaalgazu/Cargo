﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PrimerProyecto.Models;

namespace PrimerProyecto.Controllers
{
    public class ProcesosController : Controller
    {
        private PrimerProyectoEntities1 db = new PrimerProyectoEntities1();

        // GET: Procesos
        public ActionResult Index()
        {
            return View(db.Procesos.ToList());
        }

        // GET: Procesos/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Procesos procesos = db.Procesos.Find(id);
            if (procesos == null)
            {
                return HttpNotFound();
            }
            return View(procesos);
        }

        // GET: Procesos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Procesos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Nombre,Sucursal,Estado")] Procesos procesos)
        {
            if (ModelState.IsValid)
            {
                db.Procesos.Add(procesos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(procesos);
        }

        // GET: Procesos/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Procesos procesos = db.Procesos.Find(id);
            if (procesos == null)
            {
                return HttpNotFound();
            }
            return View(procesos);
        }

        // POST: Procesos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Nombre,Sucursal,Estado")] Procesos procesos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(procesos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(procesos);
        }

        // GET: Procesos/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Procesos procesos = db.Procesos.Find(id);
            if (procesos == null)
            {
                return HttpNotFound();
            }
            return View(procesos);
        }

        // POST: Procesos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Procesos procesos = db.Procesos.Find(id);
            db.Procesos.Remove(procesos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
